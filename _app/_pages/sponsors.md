---
layout: page
title: Sponsors
permalink: /sponsors/
desc: 感謝・ありがとう・Thank you
category: sponsors
---

I make many different kinds of things, WordPress themes, plugins, app skins, and websites. If you like my work, please consider buying me a cup of coffee. Thanks!

<p class="largetype">
  Sponsor via <a href="https://github.com/sponsors/sparanoid">GitHub</a> or <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=X9HJURBA62JYC&source=url">PayPal</a>
</p>
