---
layout: post
title: A New Keyboard
category: note
---

[Microsoft Comfort Curve Keyboard 2000](https://www.cnet.com/products/microsoft-comfort-curve-keyboard-2000-series/), The whole keyboard is a curved shape, comfortable, ergonomically inspired contours, and it looks a little strange. It sounds lighter than my old Logitech keyboard. The bad is the keys feel a bit mushy, especially the space key.
