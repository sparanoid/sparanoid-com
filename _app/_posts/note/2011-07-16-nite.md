---
layout: post
title: NITE
category: note
---

Last time I redesign my site [I said](/note/cyrconplex/) “Oh this time it looks great I’m going to use it for long enough”. But less than two month I redesigned my site again. It at first inspired by [Lifepath](https://web.archive.org/web/20111127185400/http://lifepath.me/), I create a [start page](/lab/start/) for my Safari default homepage (It looks really nice and you can have a try). I’m satisfied with this style then I deployed it to the whole site. The style what you’re looking at.

I can’t really get what can I do with my site, except redesigning it from time to time.
