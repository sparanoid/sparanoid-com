---
layout: post
title: find.ac Alter
category: work
tag: commercial
excerpt: find.ac Alter merchant page for a Japanese ACG (Animations, Comics and Games) community
thumbnail: thumb/find.ac-alter.png
plugin: lightense
---

![]({{ site.file }}/find.ac-alter.jpg)
{: .browser}
