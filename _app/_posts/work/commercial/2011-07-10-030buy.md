---
layout: post
title: Project 030Buy
category: work
tag: commercial
excerpt: 030Buy online shopping site for a Japanese ACG (Animations, Comics and Games) community
link: https://www.030buy.net/
scheme-text: "#ff9518"
scheme-link: "#785445"
scheme-hover: "#ff9518"
scheme-bg: "#eae2d2"
scheme-list-color: "text"
plugin: lightense
---

## Checkout Page #1
![]({{ site.file }}/030buy-01.png)
{: .browser}

## Order list
![]({{ site.file }}/030buy-04.png)
{: .browser}

## Checkout Page #1
![]({{ site.file }}/030buy-02.png)
{: .browser}
