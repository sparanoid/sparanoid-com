---
layout: post
title: Tianjin Project
category: work
tag: commercial
excerpt: A Detailed Introduction to Municipality of Tianjin
thumbnail: thumb/tianjin-project.jpg
heading-img: svg/tianjin-project.svg
heading-img-local: true
heading-img-width: 400
link: /lab/tianjin/
plugin: lightense
---

There's a [newer revision](/work/tianjin-project-revision/) of this project.
{: .note}

Now this project is featured on [The World Economic Forum - Tianjin Davos 2012](/work/tianjin-project-wef/). You can also check out the [project page](/lab/tianjin/).
{: .note}

[Download]({{ site.download }}/Tianjin.ibooks) it for iPad. (No iPad? Here’s a [PDF version]({{ site.download }}/Tianjin.pdf))
{: .download}

## Book Preview
![Tianjin Project Book Preview #6]({{ site.file }}/tianjin-project-preview-merged.jpg){: .no-enlarge}

![Tianjin Project Book Preview #1]({{ site.file }}/tianjin-project-preview-01.jpg){: .no-enlarge}

![Tianjin Project Book Preview #2]({{ site.file }}/tianjin-project-preview-02.jpg){: .no-enlarge}

![Tianjin Project Book Preview #3]({{ site.file }}/tianjin-project-preview-03.jpg){: .no-enlarge}

![Tianjin Project Book Preview #4]({{ site.file }}/tianjin-project-preview-04.jpg){: .no-enlarge}

![Tianjin Project Book Preview #5]({{ site.file }}/tianjin-project-preview-05.jpg){: .no-enlarge}

## Promo Video
You can view the promotion video on [YouTube](https://youtu.be/dk2Fg8WJ3-o).
