---
layout: post
title: Tianjin Project × WEF
category: work
tag: commercial
excerpt: A Detailed Introduction to Municipality of Tianjin featured on the World Economic Forum
thumbnail: thumb/tianjin-project-wef.jpg
heading-img: svg/tianjin-project-wef.svg
heading-img-local: true
heading-img-width: 600
svg-headline-height: 125
plugin: lightense
---

There's a [newer revision](/work/tianjin-project-revision/) of this project.
{: .note}

[Tianjin Project](/work/tianjin-project/) is now featured on the World Economic Forum (WEF) - Tianjin Davos 2012. Created using iBooks Author, for iPad only.

## Book Preview
![Tianjin Project × World Economic Forum Book Preview]({{ site.file }}/tianjin-project-wef-preview-merged.jpg){: .no-enlarge}
