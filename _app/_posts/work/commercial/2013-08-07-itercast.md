---
layout: post
title: IterCast
category: work
tag: commercial
link: /lab/itercast/
thumbnail: thumb/itercast.png
heading-img: svg/itercast.svg
heading-img-local: true
heading-img-width: 400
svg-headline-height: 100
scheme-text: "#aa2800"
scheme-link: "#ffb800"
scheme-hover: "#ecde3d"
scheme-code: "#32d5db"
scheme-bg: "#ff5420"
plugin: lightense
---

[IterCast](https://web.archive.org/web/20140207233511/http://itercast.com/) (previous LinuxCast), An an online interactive education platform that offers coding courses in programming languages like Python, JavaScript, and Objective-C, database management courses such as MySQL and Oracle, Cisco Career certifications such as CCNA and CCNP Security, as well as markup languages such as HTML and CSS. All classes are free to everyone. I do design and front-end stuff for this project.

Update: this project is now been discontinued, but you can see a cached site from my website.

![]({{ site.file }}/itercast-banners-2.png)

![]({{ site.file }}/itercast-icons-2.png)

![]({{ site.file }}/itercast-01.png)
{: .browser}

One of my favorite part of this project is the error pages, You really should check out the animated version:

[![]({{ site.file }}/itercast-02.jpg)](/lab/itercast/error-404-403.html)
{: .browser}

[![]({{ site.file }}/itercast-03.jpg)](/lab/itercast/error-503.html)
{: .browser}
