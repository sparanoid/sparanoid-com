---
layout: post
title: Tianjin Project Revision
category: work
tag: commercial
plugin: lightense
---

This is just another revision for the original [Tianjin Project](/work/tianjin-project/), optimized for recently introduced iBooks for OS X. I also redesigned a new cover for it.

[Read it on your Mac](https://itunes.apple.com/us/book/tianjin/id1050471618?mt=13).
{: .download}

## Index Overview
![Index Overview]({{ site.file }}/tianjin-revision-el-capitan-01.jpg)
{: .screenshot-mac}

## Table of Contents
![Table of Contents]({{ site.file }}/tianjin-revision-el-capitan-02.jpg)
{: .screenshot-mac}

## Bookmarks
![Bookmarks]({{ site.file }}/tianjin-revision-el-capitan-03.jpg)
{: .screenshot-mac}

## Full-context Search
![Full-context Search]({{ site.file }}/tianjin-revision-el-capitan-04.jpg)
{: .screenshot-mac}

## Sharing notes
![Sharing notes]({{ site.file }}/tianjin-revision-el-capitan-05.jpg)
{: .screenshot-mac}

## Glossary
![Glossary]({{ site.file }}/tianjin-revision-el-capitan-06.jpg)
{: .screenshot-mac}

## Glossary List
![Glossary List]({{ site.file }}/tianjin-revision-el-capitan-07.jpg)
{: .screenshot-mac}

## Study Cards
![Study Cards]({{ site.file }}/tianjin-revision-el-capitan-08.jpg)
{: .screenshot-mac}

You can also view the [original version](/work/tianjin-project/) of this project.
{: .note}
