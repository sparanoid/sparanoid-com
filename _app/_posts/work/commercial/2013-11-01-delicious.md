---
layout: post
title: Delicious Redesign
category: work
tag: commercial
link: https://web.archive.org/web/20131214223249/https://delicious.com/
scheme-text: "#0062c6"
scheme-link: "#fff"
scheme-hover: "#00edff"
scheme-code: "#32d5db"
scheme-bg: "#39f"
plugin: lightense
---

[Delicious](https://web.archive.org/web/20131214223249/https://delicious.com/) (aka. del.icio.us) is a social bookmarking web service for storing, sharing, and discovering web bookmarks that first launched back in 2003. In 2005 it was acquired by Yahoo! and later sold to AVOS Systems Inc. The website’s design hasn’t changed very much since its last redesign in 2011.

This year, to celebrate its 10th anniversary, AVOS invited me to join the Delicious team. I do designs and front-end stuff, collaborated with the developer [Ning Sun](https://sunng.info/) ([again](/work/readwise/)) from AVOS on this project.

Update 2014 May: Delicious now has been acquired by [Science Inc.](https://www.science-inc.com/) [^1]

Update 2017 June: [Pinboard](https://pinboard.in/) Acquires Delicious [^2]

## Delicious New Logo
![Delicious New Logo]({{ site.file }}/delicious.com-logo.png)

## Delicious Iconset
![Delicious Iconset]({{ site.file }}/delicious.com-iconset.png)

## Delicious UI Preview
![Delicious UI Preview]({{ site.file }}/delicious.com.png)

Some fun facts:

- Brand-new brighter version of Delicious logo. (Time took: 5 minutes)
- No images used on this project. all styles are served by a ~23 KB gzipped CSS file. Hum, okay, there’s only one icon font and some SVG graphics and…
- The new design is responsive, I even spent more time on the mobile viewport, or I can say, it’s a mobile-first design. Try it out on your iPhone, it works smooth and intuitive.
- Delicious logo is made of pure CSS.
- I didn’t write any vendor prefix for this project. (Or let me say: [Autoprefixer](https://github.com/ai/autoprefixer) rocks).
- We use Application Cache to speed up the page load times.
- We use [Grunt](https://gruntjs.com/).
- I worked remotely (from home) for all these things.
- The author of [Mobile First](https://abookapart.com/products/mobile-first), former Yahoo! design architect, [Luke Wroblewski](https://www.lukew.com/) also [tweeted](https://twitter.com/lukew/status/384744062361686017) about it.

## Delicious Landing Page
![Delicious Landing Page]({{ site.file }}/delicious.com-homepage.png)
{: .browser}

## Delicious Landing Page (Mouseover)
![Delicious Landing Page (Mouseover)]({{ site.file }}/delicious.com-homepage-hover.png)
{: .browser}

## More Screenshots: Delicious Login Window
![Delicious Login Window]({{ site.file }}/delicious.com-screenshot-01.png)
{: .browser}

## Delicious Signup Form
![Delicious Signup Form]({{ site.file }}/delicious.com-screenshot-02.png)
{: .browser}

## Delicious - Useful Tools
![Delicious - Useful Tools]({{ site.file }}/delicious.com-screenshot-03.png)
{: .browser}

## Delicious - Developing for Delicious
![Delicious - Developing for Delicious]({{ site.file }}/delicious.com-screenshot-04.png)
{: .browser}

## Delicious - About
![Delicious - About]({{ site.file }}/delicious.com-screenshot-05.png)
{: .browser}

## Delicious - My Links
![Delicious - My Links]({{ site.file }}/delicious.com-screenshot-06.png)
{: .browser}

## Delicious - Manage Subscriptions
![Delicious - Manage Subscriptions]({{ site.file }}/delicious.com-screenshot-07.png)
{: .browser}

## Delicious - Search
![Delicious - Search]({{ site.file }}/delicious.com-screenshot-08.png)
{: .browser}

## Delicious - Share a Link
![Delicious - Share a Link]({{ site.file }}/delicious.com-screenshot-09.png)
{: .browser}

## Delicious - Edit a Link
![Delicious - Edit a Link]({{ site.file }}/delicious.com-screenshot-10.png)
{: .browser}

## Delicious - Mass Edit Links
![Delicious - Mass Edit Links]({{ site.file }}/delicious.com-screenshot-11.png)
{: .browser}

## Delicious - Keyboard Shortcuts
![Delicious - Keyboard Shortcuts]({{ site.file }}/delicious.com-screenshot-12.png)
{: .browser}

[^1]: Science Inc., a tech advisory firm run by former Myspace chief Michael Jones, confirmed the deal in a [blog post](https://web.archive.org/web/20140719033531/http://science-inc.com/about/blog/2014/05/08/welcome-delicious-as-the-cornerstone-asset-of-our-new-data-content-group/).
[^2]: Pinboard has acquired Delicious. Here’s what you need to know: [Pinboard Acquires Delicious](https://blog.pinboard.in/2017/06/pinboard_acquires_delicious/).
