---
layout: post
title: LeanCloud
category: work
tag: commercial
thumbnail: thumb/avoscloud-thumb.png
link: https://leancloud.cn/
heading-img: svg/avoscloud.svg
heading-img-local: true
heading-img-width: 458
svg-headline-height: 64
scheme-text: "#666c7b"
scheme-link: "#ff8b0e"
scheme-hover: "#fa9324"
scheme-code: "#21c2e6"
scheme-bg: "#fff"
scheme-list-color: "code"
plugin: lightense
---

[AVOS Cloud](https://leancloud.cn), one of the most famous Chinese local (m)BaaS platform. Me and the whole team have been working on this project since last year. Now it's a spin-off of [AVOS Systems, Inc.](https://web.archive.org/web/20131201010958/http://www.avos.com/) focuses on tech startups and local companies in China.

I'm working on this site as a "hybird" designer to create eye-catching interactions and provide easy-to-use dashboard for every developer.

## AVOS Cloud Homepage

The background cubes are animated and written in pure CSS, the color schemes of the page can also be changed according to different slices. You can check out [this pen](https://codepen.io/sparanoid/pen/axiKF) for a simple 3D cubes demo.

![AVOS Cloud Homepage Slides #1]({{ site.file }}/avoscloud-homepage-01-cropped.png)
{: .browser}

![AVOS Cloud Homepage Slides #2]({{ site.file }}/avoscloud-homepage-02-cropped.png)
{: .browser}

![AVOS Cloud Homepage Slides #3]({{ site.file }}/avoscloud-homepage-03-cropped.png)
{: .browser}

![AVOS Cloud Homepage Slides #4]({{ site.file }}/avoscloud-homepage-04-cropped.png)
{: .browser}

![AVOS Cloud Homepage Slides #5]({{ site.file }}/avoscloud-homepage-05-cropped.png)
{: .browser}

## AVOS Cloud Features Page

A full features list providing Overview, How it Works, and Resources.

![AVOS Cloud Features Overview]({{ site.file }}/avoscloud-features-small-merged.jpg)

![AVOS Cloud Features Slides #1]({{ site.file }}/avoscloud-features-01.png)
{: .browser}

![AVOS Cloud Features Slides #2]({{ site.file }}/avoscloud-features-02.png)
{: .browser}

![AVOS Cloud Features Slides #3]({{ site.file }}/avoscloud-features-03.png)
{: .browser}

![AVOS Cloud Features Slides #4]({{ site.file }}/avoscloud-features-04.png)
{: .browser}

## AVOS Cloud Pricing Page

The pricing modal can slide out without redirecting to another page. Color schemes of this modal are dynamic, too.

![AVOS Cloud Pricing Page]({{ site.file }}/avoscloud-pricing-merged-new.jpg)

![AVOS Cloud Pricing Slides #1]({{ site.file }}/avoscloud-homepage-pricing-new-01.png)
{: .browser}

![AVOS Cloud Pricing Slides #2]({{ site.file }}/avoscloud-homepage-pricing-new-02.png)
{: .browser}

![AVOS Cloud Pricing Slides #3]({{ site.file }}/avoscloud-homepage-pricing-new-03.png)
{: .browser}

![AVOS Cloud Pricing Slides #4]({{ site.file }}/avoscloud-homepage-pricing-new-04.png)
{: .browser}

![AVOS Cloud Pricing Slides #5]({{ site.file }}/avoscloud-homepage-pricing-new-05.png)
{: .browser}
