---
layout: post
title: IKEA Stock Availability Checker
category: work
tag: project
head: |
  <link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/hjdnnkppfadnemodjjnbacnagkgbhekm">
---

## Intro

Get all stock availability for current IKEA product in your selected country.

Aug 31, 2020: IKEA has updated its website with a new API and a new method to get the stock information. We can now query stock availability more simply and cleanly. So I will no longer update this extension.

## Downloads

<div class="largetype">
  <div><a href="https://chrome.google.com/webstore/detail/hjdnnkppfadnemodjjnbacnagkgbhekm">Add to Chrome</a></div>
</div>

## Love this?

Please consider [buying me a cup of coffee]({{ '/donate/' | relative_url }}).
