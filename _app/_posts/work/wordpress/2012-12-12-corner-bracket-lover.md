---
layout: post
title: Corner Bracket Lover
category: work
tag: wordpress
excerpt: A WordPress plugin to convert all curly quotation marks in your posts to traditional corner brackets.
thumbnail: thumb/corner-bracket-lover.png
heading-img: svg/corner-bracket-lover.svg
heading-img-local: true
heading-img-width: 500
link: https://wordpress.org/plugins/corner-bracket-lover/
scheme-text: "#a79bbf"
scheme-link: "#6d52a7"
scheme-hover: "#a48cd3"
scheme-code: "#4eea6e"
scheme-bg-light: true
---

**Corner Bracket Lover** is a WordPress plugin to convert all curly quotation marks (`“”` and `‘’`) in your posts to traditional corner brackets (`「」` and `『』`). A must-have plugin if you're a Chinese writer or just a corner brackets lover. This plugin also works fine with multisite enabled WordPress (aka. WordPress Mu). If you love this plugin, please consider [buying me a cup of coffee]({{ '/donate/' | relative_url }}).

[Download](https://wordpress.org/extend/plugins/corner-bracket-lover/) it at WordPress.org
{: .download}
